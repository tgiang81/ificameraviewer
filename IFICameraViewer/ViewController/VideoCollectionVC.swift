//
//  VideoCollectionVC.swift
//  IFICameraViewer
//
//  Created by Giang on 5/27/19.
//  Copyright © 2019 Giang. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MaterialActivityIndicator

import AVFoundation

class VideoCollectionVC: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    let cellListID = "ListCell"
    let cellGridID = "GridCell"
    var isListView = false
    let disposeBag = DisposeBag()
    let indicator = MaterialActivityIndicatorView()
    var listVideos: [Video]?
    
    var toggleButton = UIBarButtonItem()

    let queue = OperationQueue()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupActivityIndicatorView()

        collectionView?.backgroundColor = UIColor.white
        toggleButton = UIBarButtonItem(title: "List", style: .plain, target: self, action: #selector(butonTapped(sender:)))
        self.navigationItem.setRightBarButton(toggleButton, animated: true)
        
        //list video
        indicator.startAnimating()
        let apiClient = APIService()
        
        apiClient.getVideos(userId: 123)
            .observeOn(MainScheduler.instance)
            .subscribe(
                onNext:{ [weak self] videos in
                    
                    self?.indicator.stopAnimating()
                    
                    print(videos)
                    
                    self?.listVideos?.removeAll()
                    self?.listVideos = videos
                    self?.collectionView.reloadData()
                }
            ).disposed(by: self.disposeBag)
    }
    
    @objc func butonTapped(sender: UIBarButtonItem) {
        if isListView {
            toggleButton = UIBarButtonItem(title: "List", style: .plain, target: self, action: #selector(butonTapped(sender:)))
            isListView = false
        }else {
            toggleButton = UIBarButtonItem(title: "Grid", style: .plain, target: self, action: #selector(butonTapped(sender:)))
            isListView = true
        }
        self.navigationItem.setRightBarButton(toggleButton, animated: true)
        self.collectionView?.reloadData()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listVideos?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let video =  listVideos?[indexPath.row]
        if isListView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellListID, for: indexPath) as! ListCell
            
            cell.labelName?.text = video?.title
            cell.labelSize?.text = video?.size
            
            //optimize operation download thumb
            DispatchQueue.global(qos: .background).async{
                if let url = URL(string:  video?.videoUrl ?? ""),
                    let thumbnailImage = self.getThumbnailImage(forUrl: url)
                {
                    DispatchQueue.main.async {
                        cell.imageView.image = thumbnailImage
                    }
                }
            }
            return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellGridID, for: indexPath) as! GridCell
            cell.labelName?.text = video?.title
            
            DispatchQueue.global(qos: .background).async{
                if let url = URL(string:  video?.videoUrl ?? ""),
                    let thumbnailImage = self.getThumbnailImage(forUrl: url)
                {
                    DispatchQueue.main.async {
                        cell.imageView.image = thumbnailImage
                    }
                }
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = view.frame.width
        
        if isListView {
            return CGSize(width: width, height: 120)
        }else {
            return CGSize(width: (width - 15)/2, height: (width - 15)/2)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
}

//MARK:- Grid cell
class GridCell: UICollectionViewCell{

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelName: UILabel!
 
}

//MARK:- List cell
class ListCell: UICollectionViewCell{
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelSize: UILabel!
    @IBOutlet weak var labelDuration: UILabel!
}

extension VideoCollectionVC {
    
    //MARK:- Indicator setting
    private func setupActivityIndicatorView() {
        view.addSubview(indicator)
        setupActivityIndicatorViewConstraints()
    }
    
    private func setupActivityIndicatorViewConstraints() {
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        indicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }
        return nil
    }

}
