//
//  LoginVC.swift
//  IFICameraViewer
//
//  Created by Giang on 5/27/19.
//  Copyright © 2019 Giang. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MaterialActivityIndicator

class LoginVC: UIViewController {
    
    @IBOutlet weak var tfUsername: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    
    let mainViewModel = LoginViewModel()
    let disposeBag = DisposeBag()
    let indicator = MaterialActivityIndicatorView()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupActivityIndicatorView()
        createViewModelBinding()
        //create callback
        createCallbacks()
    }

    func createViewModelBinding(){
        
        //bind field email -> email VM
        tfUsername.rx.text.orEmpty
            .bind(to: mainViewModel.emailIDViewModel.data)
            .disposed(by: disposeBag)
        
        //bind field password -> password VM
        tfPassword.rx.text.orEmpty
            .bind(to: mainViewModel.passwordViewModel.data)
            .disposed(by: disposeBag)
        
        /*
        //bind btnLogin's click event to...
        btnLogin.rx.tap.do(onNext: { [unowned self] in
            //resign keyboard
            self.tfUsername.resignFirstResponder()
            self.tfPassword.resignFirstResponder()
            
        }).subscribe(onNext: { [unowned self] in
            //call API login if validating successful
            if self.mainViewModel.validateLogin(){
                print("Validated!....go login")
                self.mainViewModel.loginUser()
            }
            
        }).disposed(by: disposeBag)
        */
        //Binding
        mainViewModel.isLoading.asObservable().observeOn(MainScheduler.instance)
            .subscribe( onNext:{ [weak self] bLoading in
                if bLoading {
                    self?.indicator.startAnimating()
                }else{
                    self?.indicator.stopAnimating()
                }
                }
            ).disposed(by: disposeBag)
    }
    
    func createCallbacks(){
        
        //listen to the login result
        mainViewModel.isSuccess.asObservable().observeOn(MainScheduler.instance)
            .bind{ [weak self] value in
                print("is login successful!... \(value)")
                if value {
                    self?.gotoScreenListVideo()
                }
            }.disposed(by: disposeBag)
        
        //listen to error issue
        mainViewModel.errorMsg.asObservable()
            .bind{ errMessage in
                print(errMessage)
            }.disposed(by: disposeBag)
        
        //
    }

    @IBAction func fnDoLogin(_ sender: Any) {
        if self.mainViewModel.validateLogin(){
            print("Validated!....go login")
            self.mainViewModel.loginUser()
        }
    }
}

extension LoginVC {
    
    //MARK:- Indicator setting
    private func setupActivityIndicatorView() {
        view.addSubview(indicator)
        setupActivityIndicatorViewConstraints()
    }
    
    private func setupActivityIndicatorViewConstraints() {
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        indicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    func gotoScreenListVideo() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "VideoCollectionVCID") as! VideoCollectionVC
        vc.title = "Videos"
        navigationController?.pushViewController(vc, animated: true)
    }
}

