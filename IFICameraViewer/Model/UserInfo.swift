//
//  UserInfo.swift
//  IFICameraViewer
//
//  Created by Giang on 5/27/19.
//  Copyright © 2019 Giang. All rights reserved.
//

import Foundation
/*
 "user_id": "1",
 "user_name": "Giang",
 "token": "123456788wertwertwgsdfgsfdxcvbxvbcv1"
 */

struct UserInfo: Codable {
    let id: String
    let name: String
    let token: String
    
    enum CodingKeys: String, CodingKey {
        case id = "user_id"
        case name = "user_name"
        case token
    }
}
