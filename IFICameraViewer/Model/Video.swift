//
//  Video.swift
//  IFICameraViewer
//
//  Created by Giang on 5/27/19.
//  Copyright © 2019 Giang. All rights reserved.
//
/*
 "video_id": 1,
 "video_url": "https://video-previews.elements.envatousercontent.com/h264-video-previews/1021d153-4b1b-4e21-a5d5-8c065714b49c/11912182.mp4",
 "title": "video1",
 "file_size": "100 MB",
 "duration": "1h"
 },
 
 */
import Foundation

struct Video: Codable{
    let videoId: Int
    let videoUrl: String
    let title: String
    let size: String
    let duration: String

    enum CodingKeys: String, CodingKey{
        case videoId = "video_id"
        case videoUrl = "video_url"
        case title
        case size = "file_size"
        case duration
    }
}
