//
//  ApiRouter.swift
//  IFICameraViewer
//
//  Created by Giang on 5/27/19.
//  Copyright © 2019 Giang. All rights reserved.
//

import Foundation
import Alamofire

enum ApiRouter: URLRequestConvertible{
    
    case getLogin(user: User)
    case getVideos(userId: Int)
    
    //protocol func
    func asURLRequest() throws -> URLRequest {
        let url = try Constants.baseUrl.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        //http
        urlRequest.httpMethod = method.rawValue
        
        //headers: application/json
        urlRequest.setValue(Constants.ContentType.json.rawValue, forHTTPHeaderField: Constants.HttpHeaderField.acceptType.rawValue)
        urlRequest.setValue(Constants.ContentType.json.rawValue, forHTTPHeaderField: Constants.HttpHeaderField.contentType.rawValue)

        //encoding
        let encoding: ParameterEncoding = {
            switch method {
            case .get:
                return URLEncoding.default
            default:
                return JSONEncoding.default
            }
        }()
        
        return try encoding.encode(urlRequest, with: parameters)
    }
    
    private var method: HTTPMethod{
        switch self{
            case .getVideos, .getLogin:
            return .get
        }
    }
    
    private var path: String{
        switch self {
        case .getVideos:
            //hardcode path component
            // http://www.mocky.io/v2/5cecdb89330000bfdb6d7c1d
            return "5cecdb89330000bfdb6d7c1d"
        case .getLogin:
            // http://www.mocky.io/v2/5cecb4d2330000c9bd6d7b4b
            return "5cecb4d2330000c9bd6d7b4b"
        }
    }
    
    private var parameters: Parameters? {
        switch self {
        case .getVideos(let userId):
            return [Constants.Parameters.userId : userId]
        case .getLogin(let user):
            return ["email":user.email, "password": user.password ]
        }
    }
}
