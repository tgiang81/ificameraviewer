//
//  ApiClient.swift
//  IFICameraViewer
//
//  Created by Giang on 5/27/19.
//  Copyright © 2019 Giang. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire


protocol APIServiceProtocol {
    func doLogin(user: User) -> Observable<UserInfo>
    func getVideos(userId: Int) -> Observable<[Video]>
}

class APIService: APIServiceProtocol{
    
    // MARK: - Videos
    enum GetVideoFailureReason: Int, Error {
        case unAuthorized = 401
        case notFound = 404
    }
    
    func doLogin(user: User) -> Observable<UserInfo> {
        
        return Observable.create{ observer -> Disposable in
            Alamofire.request(ApiRouter.getLogin(user: user))
                .validate()
                .responseJSON{ response in
                    switch response.result {
                    case .success:
                        guard let data = response.data else{
                            observer.onError(response.error ?? GetVideoFailureReason.notFound)
                            return
                        }
                        
                        do{
                            let userInfo = try JSONDecoder().decode(UserInfo.self, from: data)
                            observer.onNext(userInfo)
                            observer.onCompleted()
                        }catch{
                            observer.onError(error)
                        }
                    case .failure(let error):
                        observer.onError(error)
                    }
                    
            }
            return Disposables.create()
        }
    }
    
    func getVideos(userId: Int) -> Observable<[Video]> {
        
        return Observable.create { observer -> Disposable in
            
            Alamofire.request(ApiRouter.getVideos(userId: userId))
                .validate()
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        guard let data = response.data else{
                            observer.onError(response.error ?? GetVideoFailureReason.notFound)
                            return
                        }
                        
                        do{
                            let videos = try JSONDecoder().decode([Video].self, from: data)
                            observer.onNext(videos)
                            observer.onCompleted()
                        }catch{
                            observer.onError(error)
                        }
                    case .failure(let error):
                        observer.onError(error)
                    }
            }
            return Disposables.create()
        }
    }
}

class MockAPIService: APIServiceProtocol{
    func doLogin(user: User) -> Observable<UserInfo> {
        
        return Observable.create{ observer -> Disposable in
            
            //test error case
//            observer.onError(NSError(domain: "My domain", code: -1, userInfo: nil))
            
            //test success case
            observer.onNext(UserInfo(id: "1", name: "Usertest", token: "abc123121212") )
            observer.onCompleted()
            return Disposables.create()
        }
    }
    
    func getVideos(userId: Int) -> Observable<[Video]> {
        return Observable.create{ observer -> Disposable in
            
            observer.onNext([Video(videoId: 1, videoUrl: "http://", title: "Videox", size: "1M", duration: "5h")
                ,
                            Video(videoId: 2, videoUrl: "http://", title: "Videoy", size: "1M", duration: "10h")
                ])
            
            observer.onCompleted()
            return Disposables.create()
        }
    }
}
