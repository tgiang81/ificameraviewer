//
//  UserViewmodal.swift
//  IFICameraViewer
//
//  Created by Giang on 5/27/19.
//  Copyright © 2019 Giang. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

//Mark: -  Define model User
/*
 class User{
 var email = ""
 var password = ""
 
 convenience init(email: String, pass: String){
 self.init()
 self.email = email
 self.password = pass
 }
 }
 */
//
protocol ValidationUserViewModel{
    //    The variable errorMessage will hold a pre-defined error message to display to user or a message received via API response.
    var errorMessage: String{get}
    
    //Observables:    Variables value & errorValue will be our observables that we will bind to our views later on.
    var data: BehaviorRelay<String> {get}
    var errorValue: BehaviorRelay<String> {get}
    
    //    a function validateCredentials() which performs the validation as required in respective ViewModel
    func validateLogin() -> Bool
}

//Mark:- Define ViewModel for the User



////Mark:- main view model
class LoginViewModel{
    
    //dependency
    let apiService: APIServiceProtocol

    var model: User = User(email: "", password: "")
    let disposeBag = DisposeBag()
    
    //initilize all viewmodels.
    let emailIDViewModel = EmailViewModel()
    let passwordViewModel = PasswordViewModel()
    
    //things...bind to view login
    let isSuccess: BehaviorRelay<Bool> = BehaviorRelay(value: false)
    let isLoading: BehaviorRelay<Bool> = BehaviorRelay(value: false)
    let errorMsg : BehaviorRelay<String> = BehaviorRelay(value: "")
    
    //inject
    init(apiService: APIServiceProtocol = APIService() ) {
        self.apiService = apiService
    }
    
    func validateLogin() -> Bool {
        return emailIDViewModel.validateLogin() && passwordViewModel.validateLogin()
    }

    //do login task
    func loginUser(){
        //init model with filled values
        model.email = emailIDViewModel.data.value
        model.password = passwordViewModel.data.value
        self.isLoading.accept(true)

        self.apiService.doLogin(user: User(email: model.email, password: model.password))
            .subscribe(
                onNext:{ response in
                    print(response)
                    self.isLoading.accept(false)
                    self.isSuccess.accept(true)
            },
                onError:{ error in
                    self.isLoading.accept(false)
                    self.errorMsg.accept("error.message")
                    
            }
            ).disposed(by: disposeBag)
    }
}
