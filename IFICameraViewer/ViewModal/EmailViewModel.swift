//
//  EmailViewModel.swift
//  IFICameraViewer
//
//  Created by Giang on 5/27/19.
//  Copyright © 2019 Giang. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

//Mark:- EmailViewModel
class EmailViewModel : ValidationUserViewModel{
    
    //set default value for the acquired properties...conform to the protocol
    var errorMessage: String = " issue!"
    var data: BehaviorRelay<String> = BehaviorRelay(value: "")
    var errorValue: BehaviorRelay<String> = BehaviorRelay(value: "")
    
    func validateLogin() -> Bool {
        //eg: if the email input is in valid format
        guard validatePattern(text: data.value) else {
            errorValue.accept(errorMessage)
            print(errorMessage + "email")
            
            return false
        }
        
        errorValue.accept("")
        return true
    }
    
    func validatePattern(text : String) -> Bool{
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: text)
    }
    
}
