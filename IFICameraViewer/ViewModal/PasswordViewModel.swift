//
//  PasswordViewModel.swift
//  IFICameraViewer
//
//  Created by Giang on 5/27/19.
//  Copyright © 2019 Giang. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class PasswordViewModel : ValidationUserViewModel{
    
    //set default value for the acquired properties...conform to the protocol
    var errorMessage: String = " issue!"
    var data: BehaviorRelay<String> = BehaviorRelay(value: "")
    var errorValue: BehaviorRelay<String> = BehaviorRelay(value: "")
    
    func validateLength(text: String, size: (min: Int, max: Int)) -> Bool
    {
        return (size.min...size.max).contains(text.count)
    }
    
    func validateLogin() -> Bool {
        //eg: all characters password must be different
        
        guard validateLength(text: data.value, size: (5,15)) ,
            !data.value.allEqual() else {
                errorValue.accept(errorMessage)
                print(errorMessage + "password")
                return false
        }
        
        errorValue.accept("")
        
        return true
    }
}
