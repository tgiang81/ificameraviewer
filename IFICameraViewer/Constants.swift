//
//  Constants.swift
//  IFICameraViewer
//
//  Created by Giang on 5/27/19.
//  Copyright © 2019 Giang. All rights reserved.
//
//REF: https://medium.com/@marioamgad9/swift-4-2-building-a-network-layer-using-alamofire-and-rxswift-e044b5636d55

import Foundation

class Constants{
//     http://www.mocky.io/v2/5cec942a330000a19c6d7ae4
    static let baseUrl = "http://www.mocky.io/v2"
    
    //Header
    enum HttpHeaderField: String {
        case authentication = "Authorization"
        case contentType = "Content-Type"
        case acceptType = "Accept"
        case acceptEncoding = "Accept-Encoding"
    }
    
    //Content type
    enum ContentType: String {
        case json = "application/json"
    }
    
    //param query
    struct Parameters{
        static let userId = "userId"
    }
    
}
