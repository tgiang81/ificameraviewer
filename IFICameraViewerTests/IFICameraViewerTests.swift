//
//  IFICameraViewerTests.swift
//  IFICameraViewerTests
//
//  Created by Giang on 5/26/19.
//  Copyright © 2019 Giang. All rights reserved.
//

import XCTest
@testable import IFICameraViewer

class IFICameraViewerTests: XCTestCase {

    var loginVM = LoginViewModel()
    let mockAPIService = MockAPIService()


    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        //MockApiService
        loginVM = LoginViewModel.init(apiService: mockAPIService)
        
        //valid account
        loginVM.emailIDViewModel.data.accept("testuser@gmail.com")
        loginVM.passwordViewModel.data.accept("123123")
        
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testUserModel(){
        let user1 = User(email: "Giang", password: "1111")
        let user1ViewModel = LoginViewModel()
        user1ViewModel.model = user1
        
        XCTAssertEqual(user1ViewModel.model.email, "Giang")
    }

    //valid password if length 5-15
    func testPasswordVM_pass(){
        let pwdVM = PasswordViewModel()
        pwdVM.data.accept("123123")
        XCTAssertTrue(pwdVM.validateLogin())
    }
    
    func testPasswordVM_fail(){
        let pwdVM = PasswordViewModel()
        pwdVM.data.accept("1234")
        XCTAssertFalse(pwdVM.validateLogin())
    }

    //
    func testEmailVM_pass(){
        let emailVM = EmailViewModel()
        emailVM.data.accept("testuser@gmail.com")
        XCTAssertTrue(emailVM.validateLogin())
    }
    
    func testEmailVM_fail(){
        let emailVM = EmailViewModel()
        emailVM.data.accept("testuser")
        XCTAssertFalse(emailVM.validateLogin())
    }
    
    func testDoLoginSuccess(){
        //with the correct mock API service data response -> check isSuccess must = true
        loginVM.loginUser()
        XCTAssertTrue(loginVM.isSuccess.value)
    }
    
    func testDoLoginFail(){
        //with the correct mock API service data response -> check isSuccess must = true
        loginVM.loginUser()
        XCTAssertFalse(loginVM.isSuccess.value)
    }

}
